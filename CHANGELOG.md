# v0.1.2 - 2020.05.09

* Autocomplete renomeado para `dnf-cache` e movido de pasta no fonte.
* Corrigida a função `_dnf_cache_firstrun`.

# v0.1.1 - 2020.05.08

* Script `dnf-cache-update` movido para `/usr/lib` sem permissão de execução.
* Script de instalação agora copia scripts para `/usr/bin` (dnf-cache) e `/usr/lib` (dnf-cache-update).
* Incluído o campo `release` no banco de dados. ([Issue #1](https://gitlab.com/blau_araujo/dnf-cache/-/issues/1))
* Experimentalmente, estamos limitando a criação do cache apenas para as versões mais recentes dos pacotes.
* Incluída a opção `finder` para executar uma busca "fuzzy" pelos pacotes.
* Pequenas correções diversas.
