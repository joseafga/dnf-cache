[[_TOC_]]

# dnf-cache

Ferramenta de busca e pesquisa de pacotes no Fedora.

## Objetivos

O principal objetivo do `dnf-cache` é oferecer uma ferramenta de terminal leve, rápida e objetiva que possa ser utilizada para realizar diversas consultas sobre os pacotes disponíveis e instalados no Fedora.

## Como utilizar

```
dnf-cache OPÇÃO

OPÇÕES:

    help      - Exibe esta ajuda.

    update    - Cria ou atualiza o cache das buscas (requer sudo).
    clean     - Limpa backup do cache (requer sudo).
    finder    - Executa o buscador 'fuzzy' (requer a instalação do pacote 'fzf').
    list      - Lista todos os nomes de pacotes.
    installed - Lista todos os pacotes instalados.

    search [PADRÃO]

              - Realiza uma busca pelo PADRÃO, retornando
                os pacotes e descrições correspondentes ao
                padrão da busca.

                Caso nenhum nome de pacote seja fornecido,
                todos os pacotes disponíveis serão
                exibidos.

    info PACOTE

              - Exibe informações sobre PACOTE.

    depends PACOTE (não implementado)

              - Lista todas as dependências de PACOTE.

    rdepends PACOTE (não implementado)

              - Lista todos os pacotes que dependem
                de PACOTE.
```

## Instalação

O método de instalação preferencial é clonando este repositório e executando o script de instalação. Alternativamente, você pode utilizar o repositório não oficial COPR para instalar pelo `dnf`.

### Instalando pelo repositório COPR (não oficial)

> **Atenção:** este repositório pode não conter a última release!

```
sudo dnf copr enable blau/dnf-cache
sudo dnf install dnf-cache
```

### Clonando este repositório

Clone o repositório, entre na pasta criada e execute o script de instalação:

```
:~$ git clone https://gitlab.com/blau_araujo/dnf-cache.git
:~$ cd dnf-cache
:~/dnf-cache$ sudo ./install.sh
```

### Alterações feitas pelo script

* Será criada uma pasta para o cache em `/var/cache/dnf-cache`.
* O arquivo `dnf-cache` será copiado para `/usr/bin`.
* O arquivo `dnf-cache-update` será copiado para `/usr/lib`.
* O arquivo `completion/dnf-cache` será copiado para `/usr/share/bash-completion/completions/`.
* A base de dados do cache será criada.

### Atualização

**Durante esta fase de desenvolvimento, antes de atualizar, é preciso remover a instalação anterior:**

**Se você instalou pelo repositório COPR**

```
sudo dnf remove dnf-cache
``` 

**Se você clonou este repositório**

```
sudo ./uninstall.sh
```

Após remover a versão anterior, atualize o repositório e execute novamente o script `install.sh`:

```
:~/dnf-cache$ git pull origin master
:~/dnf-cache$ sudo ./install.sh
```

## Desinstalação

Entre na pasta do repositório clonado e execute o script `uninstall.sh`:

```
:~$ cd dnf-cache
:~/dnf-cache$ sudo ./uninstall.sh
```

Se você instalou pelo repositório COPR:

```
sudo dnf remove dnf-cache
```

## Implementações futuras

* Opção `depends PACOTE`: lista as dependências de PACOTE.
* Opção `rdepends PACOTE`: lista os pacotes que dependem de PACOTE.

## Como colaborar

A melhor forma de colaborar é testando e apresentando os problemas que encontrar e as suas sugestões de código nas [issues](https://gitlab.com/blau_araujo/dnf-cache/-/issues) do projeto.

## Agradecimentos

* **Geysler Niclevicz**, que fez a primeira versão do script `dnf-cache-update`, sem o qual eu levarias semanas só para entender o [manual da API do DNF](https://dnf.readthedocs.io/en/latest/index.html).
* **Sérgio Correia**, por empacotar o `dnf-cache`, testar em várias distribuições compatíveis e me ajudar com os complepentos do Bash.
* **[Comunidade Fedora Brasil](https://t.me/comunidadefedorabrasil)**, pelos testes, pelo apoio, e por acompanhar o desenvolvimento nas nossas lives.
* **[José Almeida](https://gitlab.com/joseafga)**, por enviar a [primeira MR](https://gitlab.com/blau_araujo/dnf-cache/-/merge_requests/1) do projeto!
